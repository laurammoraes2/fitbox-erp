
import * as cdk from '@aws-cdk/core'
import { FitboxAPI} from '../lib/main/app/app'


const app = new cdk.App()
const stacks = {
    FitboxAPI: new FitboxAPI(app, 'App')
}

export default stacks 


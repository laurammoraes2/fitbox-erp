
import { App, Stack } from "@aws-cdk/core"
import { makeApi } from "../api/api"
import { makeCognitoUserPool } from "../cognito/user-pool/userpool"
import { makeDynamodbTables } from "../dynamodb/dynamodb-tables"



export class FitboxAPI extends Stack {
    constructor (app: App, stackName: string){
        super(app, stackName)
        makeDynamodbTables(this)
       
        makeApi(this)
    }
}


import * as dynamodb from '@aws-cdk/aws-dynamodb'
import { Stack, RemovalPolicy } from '@aws-cdk/core'

export function makeDynamodbCustomersTables (stack: Stack){
    const customerTable = new dynamodb.Table(stack, 'CustomersTable', {
        partitionKey: {
            name: 'id', 
            type: dynamodb.AttributeType.STRING 
        },
        billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        replicationRegions: ['us-east-2'],
        tableName: 'Customer-table'
    })
    return customerTable
}
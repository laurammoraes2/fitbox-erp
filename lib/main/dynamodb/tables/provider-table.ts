import * as dynamodb from '@aws-cdk/aws-dynamodb'
import { Stack, RemovalPolicy } from '@aws-cdk/core'

export function makeDynamodbProvidersTables (stack: Stack){
    const providerTable = new dynamodb.Table(stack, 'ProvidersTable', {
        partitionKey: {
            name: 'id', 
            type: dynamodb.AttributeType.STRING 
        },
        billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        replicationRegions: ['us-east-2'],
        tableName: 'Provider-table'
    })
    return providerTable
}
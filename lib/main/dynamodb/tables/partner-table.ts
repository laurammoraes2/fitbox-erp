import * as dynamodb from '@aws-cdk/aws-dynamodb'
import { Stack, RemovalPolicy } from '@aws-cdk/core'

export function makeDynamodbPartnersTables (stack: Stack){
    const partnerTable = new dynamodb.Table(stack, 'partnersTable', {
        partitionKey: {
            name: 'id', 
            type: dynamodb.AttributeType.STRING 
        },
        billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        replicationRegions: ['us-east-2'],
        tableName: 'Partner-table'
    })
    return partnerTable
}
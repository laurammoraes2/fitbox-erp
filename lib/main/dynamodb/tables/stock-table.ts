import * as dynamodb from '@aws-cdk/aws-dynamodb'
import { Stack, RemovalPolicy } from '@aws-cdk/core'

export function makeDynamodbStocksTables (stack: Stack){
    const stockTable = new dynamodb.Table(stack, 'StocksTable', {
        partitionKey: {
            name: 'id', 
            type: dynamodb.AttributeType.STRING 
        },
        billingMode: dynamodb.BillingMode.PAY_PER_REQUEST,
        replicationRegions: ['us-east-2'],
        tableName: 'Stock-table'
    })
    return stockTable
}
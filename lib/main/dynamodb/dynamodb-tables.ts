import {Stack } from '@aws-cdk/core'
import { makeDynamodbStocksTables } from './tables/stock-table'
import { makeDynamodbProvidersTables } from './tables/provider-table'
import { makeDynamodbPartnersTables } from './tables/partner-table'
import { makeDynamodbCustomersTables } from './tables/customer-tables'







export function makeDynamodbTables (stack: Stack ){
    const stockTable = makeDynamodbStocksTables(stack)
    const providerTable = makeDynamodbProvidersTables(stack)
    const partnerTable = makeDynamodbPartnersTables(stack)
    const customerTable = makeDynamodbCustomersTables(stack)

    

    return{
        stockTable,
        providerTable,
        partnerTable,
        customerTable
    }
}
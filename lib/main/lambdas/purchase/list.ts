import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PURCHASE'

export const makeListPurchaseLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'listPurchase', {
        entry: require.resolve('../../handlers/listPurchase/list.ts'),
        functionName: 'PURCHASE-list', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
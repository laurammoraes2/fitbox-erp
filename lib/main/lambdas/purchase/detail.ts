import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PURCHASE'

export const makeDetailPurchaseLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'detailPurchase', {
        entry: require.resolve('../../handlers/detailPurchase/detail.ts'),
        functionName: 'PURCHASE-detail', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PURCHASE'

export const makeUpdatePurchaseLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'updatePurchase', {
        entry: require.resolve('../../handlers/updatePurchase/update.ts'),
        functionName: 'PURCHASE-update', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
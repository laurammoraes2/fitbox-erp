import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PURCHASE'

export const makeCreatePurchaseLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'createPurchase', {
        entry: require.resolve('../../handlers/createPurchase/create.ts'),
        functionName: 'PURCHASE-create', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PURCHASE'

export const makeDeletePurchaseLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'deletePurchase', {
        entry: require.resolve('../../handlers/deletePurchase/delete.ts'),
        functionName: 'PURCHASE-delete', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
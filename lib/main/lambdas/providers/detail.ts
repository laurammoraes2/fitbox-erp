import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PROVIDER'

export const makeDetailProviderLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'detailProvider', {
        entry: require.resolve('../../handlers/detailProvider/detail.ts'),
        functionName: 'PROVIDER-detail-provider', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}   
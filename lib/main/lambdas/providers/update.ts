import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PROVIDER'

export const makeUpdateProviderLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'updateProvider', {
        entry: require.resolve('../../handlers/updateProvider/update.ts'),
        functionName: 'PROVIDER-update-provider', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}  
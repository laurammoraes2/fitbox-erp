import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PROVIDER'

export const makeCreateProviderLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'createProvider', {
        entry: require.resolve('../../handlers/createProvider/create.ts'),
        functionName: 'PROVIDER-Create-Provider', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
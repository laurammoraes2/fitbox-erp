import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PROVIDER'

export const makeListProviderLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'listProvider', {
        entry: require.resolve('../../handlers/listProvider/list.ts'),
        functionName: 'PROVIDER-list-provider', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}   
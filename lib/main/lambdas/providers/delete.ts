import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PROVIDER'

export const makeDeleteProviderLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'deleteProvider', {
        entry: require.resolve('../../handlers/deleteProvider/delete.ts'),
        functionName: 'PROVIDER-delete-provider', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}   
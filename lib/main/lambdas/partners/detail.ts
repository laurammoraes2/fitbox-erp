import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PARTNER'

export const makeDetailPartnerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'detailPartner', {
        entry: require.resolve('../../handlers/detailPartner/detail.ts'),
        functionName: 'PARTNER-Detail-Partner', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
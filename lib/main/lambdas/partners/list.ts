import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PARTNER'

export const makeListPartnerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'listPartner', {
        entry: require.resolve('../../handlers/listPartner/list.ts'),
        functionName: 'PARTNER-List-Partner', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
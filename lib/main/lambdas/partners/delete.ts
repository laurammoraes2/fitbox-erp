import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PARTNER'

export const makeDeletePartnerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'deletePartner', {
        entry: require.resolve('../../handlers/deletePartner/delete.ts'),
        functionName: 'PARTNER-Delete-Partner', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
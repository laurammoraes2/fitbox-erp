import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PARTNER'

export const makeUpdatePartnerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'updatePartner', {
        entry: require.resolve('../../handlers/updatePartner/update.ts'),
        functionName: 'PARTNER-Update-Partner', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
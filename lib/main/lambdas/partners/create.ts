import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-PARTNER'

export const makeCreatePartnerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'createPartner', {
        entry: require.resolve('../../handlers/createPartner/create.ts'),
        functionName: 'PARTNER-Create-Partner', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}     
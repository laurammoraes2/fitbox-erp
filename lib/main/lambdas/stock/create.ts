import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-STOCK'

export const makeCreateProductLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'createProduct', {
        entry: require.resolve('../../handlers/createProduct/create.ts'),
        functionName: 'STOCK-Create-Product', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}    
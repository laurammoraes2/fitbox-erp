import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-STOCK'

export const makeListProductLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'listProduct', {
        entry: require.resolve('../../handlers/listProduct/list.ts'),
        functionName: 'STOCK-list-product', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}   
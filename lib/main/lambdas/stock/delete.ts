import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-STOCK'

export const makeDeleteProductLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'deleteProduct', {
        entry: require.resolve('../../handlers/deleteProduct/delete.ts'),
        functionName: 'STOCK-delete-product', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}   
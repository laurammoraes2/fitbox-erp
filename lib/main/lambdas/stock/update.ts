import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-STOCK'

export const makeUpdateProductLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'updateProduct', {
        entry: require.resolve('../../handlers/updateProduct/update.ts'),
        functionName: 'STOCK-update-product', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}   
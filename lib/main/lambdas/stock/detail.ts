import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-STOCK'

export const makeDetailProductLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'detailProduct', {
        entry: require.resolve('../../handlers/detailProduct/detail.ts'),
        functionName: 'STOCK-detail-product', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}   
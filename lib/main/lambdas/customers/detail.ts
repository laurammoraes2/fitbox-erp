import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-CUSTOMER'

export const makeDetailCustomerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'detailsCustomer', {
        entry: require.resolve('../../handlers/detailCustomer/detail.ts'),
        functionName: 'CUSTOMER-Detail-Customer', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}     
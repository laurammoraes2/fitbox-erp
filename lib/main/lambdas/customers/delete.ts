import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-CUSTOMER'

export const makeDeleteCustomerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'deleteCustomer', {
        entry: require.resolve('../../handlers/deleteCustomer/delete.ts'),
        functionName: 'CUSTOMER-delete-customer', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}     
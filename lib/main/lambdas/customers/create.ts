import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-CUSTOMER'

export const makeCreateCustomerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'createCustomer', {
        entry: require.resolve('../../handlers/createCustomer/create.ts'),
        functionName: 'CUSTOMER-Create-Customer', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}     
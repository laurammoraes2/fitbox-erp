import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-CUSTOMER'

export const makeListCustomerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'listCustomer', {
        entry: require.resolve('../../handlers/listCustomer/list.ts'),
        functionName: 'CUSTOMER-List-Customer', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}     
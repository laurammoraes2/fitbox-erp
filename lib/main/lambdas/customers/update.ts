import {Stack} from '@aws-cdk/core'
import { TypeScriptFunction }  from 'cdk-typescript-tooling'
import { makeDynamodbReadandWrite} from '../../policies/dynamodb-read-and-write-CUSTOMER'

export const makeUpdateCustomerLambda = (stack: Stack) => {
    return new TypeScriptFunction(stack, 'updateCustomer', {
        entry: require.resolve('../../handlers/updateCustomer/update.ts'),
        functionName: 'CUSTOMER-Update-Customer', 
        initialPolicy:[makeDynamodbReadandWrite()]
    })
}     
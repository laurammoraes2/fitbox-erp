import { RestApi, LambdaIntegration, Authorizer, AuthorizationType, Resource } from "@aws-cdk/aws-apigateway"
import { Stack} from '@aws-cdk/core'
    
import {makeCreatePurchaseLambda} from '../lambdas/purchase/create'
import {makeUpdatePurchaseLambda} from '../lambdas/purchase/update'
import {makeDetailPurchaseLambda} from '../lambdas/purchase/detail'
import {makeListPurchaseLambda} from '../lambdas/purchase/list'
import {makeDeletePurchaseLambda} from '../lambdas/purchase/delete'


export const PushaseRoutes = (api: RestApi, stack: Stack): void => {
    const purchase = api.root.addResource('purchase')
    purchase.addCorsPreflight({
        allowHeaders: ['*'],
        allowOrigins: [ '*'],
        allowMethods: ['*']
    })
    createPurchase(stack, purchase)
    listPurchase(stack, purchase)
    detailPurchase(stack, purchase)
}


const createPurchase = (stack: Stack, purchaseResource: Resource) => {
    const makeCreatePurchase =  makeCreatePurchaseLambda(stack)
    purchaseResource.addMethod('PUT', new LambdaIntegration(makeCreatePurchase))  
} 
const listPurchase = (stack: Stack,  purchaseResource: Resource) => {
    const makeListPurchase =  makeListPurchaseLambda(stack)
    purchaseResource.addMethod('GET', new LambdaIntegration(makeListPurchase))  
} 
const detailPurchase= (stack: Stack,  purchaseResource: Resource) => {
    const purchase =  purchaseResource.addResource('{purchase_id}') 
    const makeDetailPurchase =  makeDetailPurchaseLambda(stack)
    purchase.addMethod('GET', new LambdaIntegration(makeDetailPurchase)) 
    const makeUpdatePurchase = makeUpdatePurchaseLambda(stack)
    purchase.addMethod('PUT', new LambdaIntegration(makeUpdatePurchase)) 
    const makeDeletePurchase =  makeDeletePurchaseLambda(stack)
    purchase.addMethod('DELETE', new LambdaIntegration(makeDeletePurchase))  
} 
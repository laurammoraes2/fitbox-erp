import { RestApi, LambdaIntegration, Authorizer, AuthorizationType, Resource } from "@aws-cdk/aws-apigateway"
import { Stack} from '@aws-cdk/core'
    
import {makeCreateProviderLambda} from '../lambdas/providers/create'
import {makeUpdateProviderLambda} from '../lambdas/providers/update'
import {makeDetailProviderLambda} from '../lambdas/providers/detail'
import {makeListProviderLambda} from '../lambdas/providers/list'
import {makeDeleteProviderLambda} from '../lambdas/providers/delete'


export const ProviderRoutes = (api: RestApi, stack: Stack): void => {
    const provider = api.root.addResource('providers')
    provider.addCorsPreflight({
        allowHeaders: ['*'],
        allowOrigins: [ '*'],
        allowMethods: ['*']
    })
    createProvider(stack, provider)
    detailProvider(stack, provider )
    listProvider(stack, provider)
}


const createProvider = (stack: Stack, providerResource: Resource) => {
    const makeCreateProvider =  makeCreateProviderLambda(stack)
    providerResource.addMethod('PUT', new LambdaIntegration(makeCreateProvider))  
} 
const listProvider = (stack: Stack,  providerResource: Resource) => {
    const makeListProvider =  makeListProviderLambda(stack)
    providerResource.addMethod('GET', new LambdaIntegration(makeListProvider))  
} 
const detailProvider = (stack: Stack,  providerResource: Resource) => {
    const provider =  providerResource.addResource('{provider_id}') 
    const makeDetailProvider =  makeDetailProviderLambda(stack)
    provider.addMethod('GET', new LambdaIntegration(makeDetailProvider)) 
    const makeUpdateProvider = makeUpdateProviderLambda(stack)
    provider.addMethod('PUT', new LambdaIntegration(makeUpdateProvider)) 
    const makeDeleteProvider =  makeDeleteProviderLambda(stack)
    provider.addMethod('DELETE', new LambdaIntegration(makeDeleteProvider))  
} 
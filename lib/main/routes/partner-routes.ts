import { RestApi, LambdaIntegration, Authorizer, AuthorizationType, Resource } from "@aws-cdk/aws-apigateway"
import { Stack} from '@aws-cdk/core'
    
import {makeCreatePartnerLambda} from '../lambdas/partners/create'
import {makeUpdatePartnerLambda} from '../lambdas/partners/update'
import {makeDetailPartnerLambda} from '../lambdas/partners/detail'
import {makeListPartnerLambda} from '../lambdas/partners/list'
import {makeDeletePartnerLambda} from '../lambdas/partners/delete'


export const PartnerRoutes = (api: RestApi, stack: Stack): void => {
    const partner = api.root.addResource('partner')
    partner.addCorsPreflight({
        allowHeaders: ['*'],
        allowOrigins: [ '*'],
        allowMethods: ['*']
    })
    createPartner(stack, partner)
    detailPartner(stack, partner )
    listPartner(stack, partner)
}


const createPartner = (stack: Stack, partnerResource: Resource) => {
    const makeCreatePartner =  makeCreatePartnerLambda(stack)
    partnerResource.addMethod('PUT', new LambdaIntegration(makeCreatePartner))  
} 
const listPartner = (stack: Stack,  partnerResource: Resource) => {
    const makeListPartner =  makeListPartnerLambda(stack)
    partnerResource.addMethod('GET', new LambdaIntegration(makeListPartner))  
} 
const detailPartner = (stack: Stack,  partnerResource: Resource) => {
    const partner =  partnerResource.addResource('{partner_id}') 
    const makeDetailPartner =  makeDetailPartnerLambda(stack)
    partner.addMethod('GET', new LambdaIntegration(makeDetailPartner)) 
    const makeUpdatePartner = makeUpdatePartnerLambda(stack)
    partner.addMethod('PUT', new LambdaIntegration(makeUpdatePartner)) 
    const makeDeletePartner =  makeDeletePartnerLambda(stack)
    partner.addMethod('DELETE', new LambdaIntegration(makeDeletePartner))  
} 
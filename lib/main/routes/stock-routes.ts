import { RestApi, LambdaIntegration, Authorizer, AuthorizationType, Resource } from "@aws-cdk/aws-apigateway"
import { Stack} from '@aws-cdk/core'
    
import {makeCreateProductLambda} from '../lambdas/stock/create'
import {makeUpdateProductLambda} from '../lambdas/stock/update'
import {makeDetailProductLambda} from '../lambdas/stock/detail'
import {makeListProductLambda} from '../lambdas/stock/list'
import {makeDeleteProductLambda} from '../lambdas/stock/delete'


export const StockRoutes = (api: RestApi, stack: Stack): void => {
    const stock = api.root.addResource('stocks')
    stock.addCorsPreflight({
        allowHeaders: ['*'],
        allowOrigins: [ '*'],
        allowMethods: ['*']
    })
    createProduct(stack, stock)
    detailProduct(stack, stock )
    listProduct(stack, stock)
}


const createProduct = (stack: Stack, stockResource: Resource) => {
    const makeCreateAuction =  makeCreateProductLambda(stack)
    stockResource.addMethod('PUT', new LambdaIntegration(makeCreateAuction))  
} 
const listProduct = (stack: Stack,  stockResource: Resource) => {
    const makeCreateAuction =  makeListProductLambda(stack)
    stockResource.addMethod('GET', new LambdaIntegration(makeCreateAuction))  
} 
const detailProduct = (stack: Stack,  stockResource: Resource) => {
    const product =  stockResource.addResource('{product_id}') 
    const makeDetailProduct =  makeDetailProductLambda(stack)
    product.addMethod('GET', new LambdaIntegration(makeDetailProduct)) 
    const makeUpdateProduct =  makeUpdateProductLambda(stack)
    product.addMethod('PUT', new LambdaIntegration(makeUpdateProduct)) 
    const makeDeleteProduct =  makeDeleteProductLambda(stack)
    product.addMethod('DELETE', new LambdaIntegration(makeDeleteProduct))  
} 
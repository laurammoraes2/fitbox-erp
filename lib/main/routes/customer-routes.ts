import { RestApi, LambdaIntegration, Authorizer, AuthorizationType, Resource } from "@aws-cdk/aws-apigateway"
import { Stack} from '@aws-cdk/core'
    
import {makeCreateCustomerLambda} from '../lambdas/customers/create'
import {makeUpdateCustomerLambda} from '../lambdas/customers/update'
import {makeDetailCustomerLambda} from '../lambdas/customers/detail'
import {makeListCustomerLambda} from '../lambdas/customers/list'
import {makeDeleteCustomerLambda} from '../lambdas/customers/delete'


export const CustomerRoutes = (api: RestApi, stack: Stack): void => {
    const customer = api.root.addResource('customer')
    customer.addCorsPreflight({
        allowHeaders: ['*'],
        allowOrigins: [ '*'],
        allowMethods: ['*']
    })
    createCustomer(stack, customer)
    detailCustomer(stack, customer )
    listCustomer(stack, customer)
}


const createCustomer = (stack: Stack, customerResource: Resource) => {
    const makeCreateCustomer =  makeCreateCustomerLambda(stack)
    customerResource.addMethod('PUT', new LambdaIntegration(makeCreateCustomer))  
} 
const listCustomer = (stack: Stack,  customerResource: Resource) => {
    const makeListCustomer =  makeListCustomerLambda(stack)
    customerResource.addMethod('GET', new LambdaIntegration(makeListCustomer))  
} 
const detailCustomer= (stack: Stack,  customerResource: Resource) => {
    const customer =  customerResource.addResource('{customer_id}') 
    const makeDetailCustomer =  makeDetailCustomerLambda(stack)
    customer.addMethod('GET', new LambdaIntegration(makeDetailCustomer)) 
    const makeUpdateCustomer = makeUpdateCustomerLambda(stack)
    customer.addMethod('PUT', new LambdaIntegration(makeUpdateCustomer)) 
    const makeDeleteCustomer =  makeDeleteCustomerLambda(stack)
    customer.addMethod('DELETE', new LambdaIntegration(makeDeleteCustomer))  
} 
import { UserPool, AccountRecovery, StringAttribute, VerificationEmailStyle } from '@aws-cdk/aws-cognito'
import {Stack} from '@aws-cdk/core'
import { makeCreateUserLambda } from '../../lambdas/user/create'
import { makeCognitoUserPoolClient} from '../user-pool-client/userPoolClient'

export const makeCognitoUserPool = (stack: Stack) => {
  const userPool = new UserPool(stack, "HitBizUserPool", { 
    selfSignUpEnabled: true, // Allow users to sign up
    autoVerify: { email: true }, // Verify email addresses by sending a verification code
    signInAliases: { email: true }, // Set email as an alias
    userVerification: {
      emailSubject: 'Verify your email for our awesome app!',
      emailBody: 'Thanks for signing up to our awesome app! Your verification code is {####}',
      emailStyle: VerificationEmailStyle.CODE,
    },
    userInvitation: {
      emailSubject: 'Invite to join our awesome app!',
      emailBody: 'Hello {username}, you have been invited to join our awesome app! Your temporary password is {####}',
    },
    standardAttributes: {
      fullname: {
        required: true,
        mutable: false,
      },
      address: {
        required: false,
        mutable: true,
      },
      profilePicture:{
        required: false, 
        mutable: true
      },
      locale:{
        required: true, 
        mutable: true,
      },
      email:{
        required: true, 
        mutable: true
      },
      profilePage:{
        required: false, 
        mutable: true
      },
     
      website:{
        required: false, 
        mutable: true
      }
    },
    customAttributes: {
      'artistType': new StringAttribute({ minLen: 3, maxLen: 15, mutable: true }) ,
      'artistHeadline': new StringAttribute({ minLen: 3, maxLen: 30, mutable: true }) ,
      'artistAbout': new StringAttribute({ minLen: 3, maxLen: 50, mutable: true }) ,
      'artistRole': new StringAttribute({ minLen: 3, maxLen: 15, mutable: true }) ,
      'artistGenre': new StringAttribute({ minLen: 3, maxLen: 15, mutable: true }) ,
    },
   
    passwordPolicy: {
      minLength: 6,
      requireLowercase: true,
      requireUppercase: true,
      requireDigits: true,
      requireSymbols: true,
     
    },
    accountRecovery: AccountRecovery.EMAIL_ONLY,
    
    lambdaTriggers: {
      preSignUp: makeCreateUserLambda,
      
    },
    userPoolName:"HitBizUserPool",
    
   

  })

 

 
  const domain = userPool.addDomain('Domain', {
    cognitoDomain: {
      domainPrefix: 'testehit',
    },
  });
  const signInUrl = domain.signInUrl(makeCognitoUserPoolClient, {
    redirectUri: 'https://example.com/home', // must be a URL configured under 'callbackUrls' with the client
  })
}
import { APIGatewayProxyEvent, APIGatewayProxyResult, } from "aws-lambda";
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: "us-east-2"});


  export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log(event)
    let body = JSON.parse(event.body)
    let user = event.requestContext.identity.user
    let id = user + body.name
    
    var data = {
        TableName: 'Customer-table',
        Key: {
            id: event.pathParameters.customer_id
        }
    }
    const response = await docClient.get(data).promise()


    
     var params = {
         TableName: 'Customer-table', 
         Item:{
            id: event.pathParameters.customer_id, 
            customerName: body.name, 
            customerTelphone: body.telphone,
            createdDate: response.Item.createdDate,
            updatedDate: Date.now(),
         }
     }
     try{ 
         const update = await docClient.put(params).promise();
        
         
         return {
            statusCode: 200, 
            body: "Produto atualizado com sucesso!",
        };
     }catch(error){
         console.error(error)
     }


  } 
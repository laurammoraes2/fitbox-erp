import { APIGatewayProxyEvent, APIGatewayProxyResult, } from "aws-lambda";
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: "us-east-2"});


  export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log(event)
    let body = JSON.parse(event.body)
    
    
     var params = {
         TableName: 'Customer-table', 
         Key: {
             id: event.pathParameters.customer_id
         },
         
     }
     
     try{ 
         const create = await docClient.delete(params).promise();
        
         
         return {
            statusCode: 200, 
            body: "Parceiro deletado com sucesso!",
        };
     }catch(error){
         console.error(error)
     }


  } 
import { APIGatewayProxyEvent, APIGatewayProxyResult, } from "aws-lambda";
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: "us-east-2"});


  export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log(event)
    let body = JSON.parse(event.body)
    let user = event.requestContext.identity.user
    let id = body.name + body.company

    var busca = {
        TableName: 'Partner-table', 
        
    }
    let verifica = 0
    const response = await docClient.scan(busca).promise()
    if(response.Items.length>0){
         verifica =await verificarId(id, response)
    }else{
         verifica = 2
    }
   
   
   
    if(verifica<1000){
        var params = {
            TableName: 'Partner-table', 
            Item:{
                id: body.name + body.company,
                partnerName: body.name, 
                partnerDescription: body.description, 
                partnerTelphone: body.telphone,
                partnerLawer: body.company,
                createdDate: Date.now(),
            }
        }
        try{ 
            const create = await docClient.put(params).promise();
           
            
            return {
               statusCode: 200, 
               body: "Parceiro criado com sucesso!",
           };
        }catch(error){
            console.error(error)
        }
    }else{
        return {
            statusCode: 200, 
            body: "Parceiro com esse id já existe!",
        };
    }
    


  } 
  async function verificarId(id: any, response: any) { 
   
    let index = 0
   
    while ( index < response.Items.length ) {
        
       if(response.Items[index].id === id){
           index = 1000
       }
     index++
    }
    return index
}
import { APIGatewayProxyEvent, APIGatewayProxyResult, } from "aws-lambda";
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: "us-east-2"});


  export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log(event)
    let body = JSON.parse(event.body)
    
    
    var params = {
        TableName: 'Stock-table', 
        Key: {
            id: event.pathParameters.product_id
        },
        
    }
    
     try{ 
         const create = await docClient.get(params).promise();
        
         
         return {
            statusCode: 200, 
            body: JSON.stringify(create),
        };
     }catch(error){
         console.error(error)
     }


  } 



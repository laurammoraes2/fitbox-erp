import { APIGatewayProxyEvent, APIGatewayProxyResult, } from "aws-lambda";
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: "us-east-2"});


  export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log(event)
    let body = JSON.parse(event.body)
  
    
     var params = {
         TableName: 'Provider-table', 
         Key:{
             id: event.pathParameters.provider_id
         },
         Item:{
            id: event.pathParameters.provider_id,
             providerName: body.name, 
             providerBrand: body.brand, 
             providerDescription: body.description,
             providerTelphone: body.telphone,
             providerAdress: body.adress,
             providerEmail: body.email, 
             providerProduct: body.products,
             updatedDate: Date.now(),
         }
     }
     try{ 
         const update = await docClient.put(params).promise();
        
         
         return {
            statusCode: 200, 
            body: "Fornecedor atualizado com sucesso!",
        };
     }catch(error){
         console.error(error)
     }


  } 
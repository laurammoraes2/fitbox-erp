import { APIGatewayProxyEvent, APIGatewayProxyResult, } from "aws-lambda";
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: "us-east-2"});


  export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log(event)
    let body = JSON.parse(event.body)
    let user = event.requestContext.identity.user
    let id = user + body.name
   
    
     var params = {
         TableName: 'Stock-table', 
         Item:{
             id: body.name + body.brand,
             productName: body.name, 
             productDescription: body.description, 
             productFornecedor: body.fornecedor,
             productPrice: body.price,
             productQtd: body.qtd,
             productDateValidade: body.validade, 
             createdDate: Date.now(),
         }
     }
     try{ 
         const create = await docClient.put(params).promise();
        
         
         return {
            statusCode: 200, 
            body: "Produto criado com sucesso!",
        };
     }catch(error){
         console.error(error)
     }


  } 





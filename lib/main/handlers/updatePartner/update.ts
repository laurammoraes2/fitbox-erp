import { APIGatewayProxyEvent, APIGatewayProxyResult, } from "aws-lambda";
const AWS = require("aws-sdk");
const docClient = new AWS.DynamoDB.DocumentClient({region: "us-east-2"});


  export const handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
    console.log(event)
    let body = JSON.parse(event.body)

    var params = {
        TableName: 'Partner-table', 
        Key: {
            id: event.pathParameters.partner_id
        },
        
    }
    const create = await docClient.get(params).promise();
        
    
    
    
     var data = {
         TableName: 'Partner-table', 
         Key: {
            id: event.pathParameters.partner_id
        },
         Item:{
            id: event.pathParameters.partner_id,
             partnerName: body.name, 
             partnerDescription: body.description, 
             partnerTelphone: body.telphone,
             partnerLawer: body.company,
             updatedDate: Date.now(),
             createdDate: create.Item.createdDate,
         }
     }
     try{ 
         const update = await docClient.put(data).promise();
        
         
         return {
            statusCode: 200, 
            body: "Parceiro atualizado com sucesso!",
        };
     }catch(error){
         console.error(error)
     }


  } 

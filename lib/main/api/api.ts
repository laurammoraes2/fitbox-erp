import { Stack } from '@aws-cdk/core'
import { RestApi } from '@aws-cdk/aws-apigateway'
import { StockRoutes } from '../routes/stock-routes'
import { PartnerRoutes } from '../routes/partner-routes'
import { ProviderRoutes } from '../routes/provider-routes'
import { CustomerRoutes } from '../routes/customer-routes'


import { UserPool } from '@aws-cdk/aws-cognito'


export function makeApi ( stack: Stack){
    const api = new RestApi(stack, 'fitboxAPI', {
        restApiName: 'FitBox API'
    })
   
    StockRoutes(api, stack),
    PartnerRoutes(api, stack),
    ProviderRoutes(api, stack),
    CustomerRoutes(api, stack)

  
    
    return api
}
    
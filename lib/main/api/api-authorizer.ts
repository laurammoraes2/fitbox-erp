import { Stack } from '@aws-cdk/core'
import { CognitoUserPoolsAuthorizer } from '@aws-cdk/aws-apigateway'
import { UserPool} from '@aws-cdk/aws-cognito'

export function makeFitBoxApiAuthorizer (stack: Stack, userPool: UserPool){
    const authorizer = new CognitoUserPoolsAuthorizer(stack, 'FitBoxAuthorizer', {
        cognitoUserPools: [
            userPool
        ],
        authorizerName: 'FitBoxauthorizer'
    })
    return authorizer
}